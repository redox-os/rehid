pub use hidreport;

pub mod report_desc;
pub mod report_handler;
pub mod usage_tables;
